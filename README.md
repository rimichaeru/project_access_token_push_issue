# project_access_token_push_issue


GitLab issue example for: https://gitlab.com/gitlab-org/gitlab/-/issues/468899

Resolution: There's an interaction for specifically protected variables, they must be specifically used in a protected tag otherwise the pipeline will break


### Issue:
Project Access Token cannot be used to push to a repo from a pipeline when the pipeline is triggered from a tag/release.

Although, the same configuration works completely fine if NOT from a tag/release.

Note that the GITLAB config vars remain the same throughout.